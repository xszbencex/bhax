<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>EPAM: Interfész evolúció Java ban</title>
        <para>
            <command>Mutasd be milyen változások történtek Java 7 és Java 8 között az interfészekben. Miért volt erre szükség, milyen problémát vezetett ez be?</command>
        </para>
        <para>
            A Java nyelv egyik nagyon hasznos feature-e az, hogy visszafele kompatibilis a régebbi verzióival, de ennek néha megvan a hátránya is. Jó példa erre, az interfészek esete,
            mivel eredetileg az interfészeknek az volt a szerepük, hogy csak abstract metódusai lehettek, de a Java 8-as verziója óta a kompatibilitás miatt
            default metúdus implementációkat és static metódusokat is megadhatunk az interfészekben.
        </para>
        <para>
            Ezzel a legnagyobb gond az volt, hogy a többszörös öröklődési problémát (diamond problem) is bevezették, aminek eredményeként repetetívvé válhatott a kódunk,
            másrészről elvesztette a sajátosságát az interface és így kevesebb sajátos tulajdonsága lett az abstract classokkal szemben.
        </para>
    </section>

    <section xml:id="liskov.epam">
        <title>EPAM: Liskov féle helyettesíthetőség elve, öröklődés</title>
        <para>
            <command>Adott az alábbi osztály hierarchia: class Vehicle, class Car extends Vehicle, class Supercar extends Car.
                Mindegyik osztály konstruktorában történik egy kiíratás, valamint a Vehicle osztályban szereplő start() metódus, minden alosztályban felül van definiálva.
                Mi történik ezen kódok futtatása esetén, és miért?</command>
        </para>
        <para>
            A feladatnak megfelelő kód: <link xlink:href="Liskov/LiskovPelda.java">Liskov/LiskovPelda.java</link>
        </para>
        <programlisting language="java"><![CDATA[// Kimenet:
I am a vehicle.
I am a car
I am a Supercar
Supercar started
true
Supercar started
true]]>
        </programlisting>
        <para>
            Az első három sort a firstVehicle példányosításánál kaptuk, mivel mindhárom class konstruktorában van kiíratás, ami az adott típusú objektum létrehozásakor hajtódik végre.
            Azért hívódik meg mind a 3 class konstruktora, mert a Supercar leszármazottja a Car-nak, az pedig a Vehicle-nek és ha létrehozunk egy leszármazott class típusú objektumot, akkor meghívódnak a szülő konstruktorok is.
            A sorrendet is érdemes megnézni, a szülőosztálytól haladunk a gyerekosztályok konstruktora felé.
        </para>
        <para>
            A Supercar started egyérteműen a firstVehicle.start(); hatására lett kiírva, mivel Supercar objektumot hoztunk létre, ezért az ott felüldefiniált metódus eredményét kapjuk meg.
        </para>
        <para>
            A (firstVehicle instanceof Car)-ra true-t kapunk, hiszen a Supercar a Car leszármazottja így annak is a példány és ezt az is alátámasztja, hogy a Car konstruktora is meghívódott.
        </para>
        <para>
            Ezek után ugyan castoljuk a firstVehicle-t Car típusúra, de a Liskov elv miatt a secondVehicle.start()-nál ugyanúgy a Supercar start() metódusa hívódik meg és még mindig leszármazottja lesz a Supercarnak az objektum.
        </para>
        <para>
            Az előző két sorban upcastolás történt, ami engedélyezett, de viszont a pédaprogramunk utolsó két sora hibát dobna, hiszen downcastolást nem lehet ilyen módon végezni.
        </para>
    </section>

    <section>
        <title>EPAM: Interfész, Osztály, Absztrak Osztály</title>
        <para>
            <command>Mi a különbség Java-ban a Class, Abstract Class és az Interface között? Egy tetszőleges példával / példa kódon keresztül mutasd be őket és hogy mikor melyik koncepciót célszerű használni.</command>
        </para>
        <para>
            Példaprogramok: <link xlink:href="Liskov/interfesz/AbstractClassPelda.java">Liskov/interfesz/AbstractClassPelda.java</link> és
            <link xlink:href="Liskov/interfesz/InterfacePelda.java">Liskov/interfesz/InterfacePelda.java</link>
        </para>
        <para>
            A 3 fogalom összehasonlításához nézzük meg először azt, hogy mik is ezek. A class egy "tervrajz", amikből objektumokat tudunk készíteni. A class nem csak deklarálja a
            tagokat és metódusokat, hanem definiálja is azokat, tehát a classból már egy működő dolgot tudunk létrehozni. Az interfészek és absztrakt classok hasonlóak a classokhoz,
            de ők az absztrakcióért felelnek, ami azt jelenti, hogy "eldugjuk" az implementációját egy feature-nek a user elől és csak a funkcionalitást szolgáltatjuk neki.
            Tehát az interfészekben és absztrakt osztályokban nem implementáljuk a működést, csak egy vázat adunk meg, amikhez az osztályok fognak funkcionalitást szolgáltatni.
        </para>
        <para>
            Célszerű az absztrakt osztály és az interfész között megnézni a különbséget, hiszen mindkettő az absztrakcióért felel, de bizonyos tulajdonságokban különböznek.
        </para>
        <itemizedlist>
            <listitem>
                <para>Az interfészeknek csak absztrakt metódusai lehetnek (ez java 8-ban megváltozott), míg az absztrakt osztályokban lehetnek nem-absztrakt tehát implementációt is tartalmazó metódusok.</para>
            </listitem>
        </itemizedlist>
        <itemizedlist>
            <listitem>
                <para>Az interfészekben csak final, static és public változók szerepelhetnek (ez java 8 és 9-ben megváltozott a visszafele kompatibilitás miatt), ellenben az absztrakt osztályokban a classoknál megismert összes lehetőség adott.</para>
            </listitem>
        </itemizedlist>
        <itemizedlist>
            <listitem>
                <para>Egy absztrakt osztály implementálhat egy interfészt, de ez fordítva nem igaz.</para>
            </listitem>
        </itemizedlist>
        <itemizedlist>
            <listitem>
                <para>Az intrefészt implementáljuk (implements), viszont az absztrakt osztályt örököljük (extends).</para>
            </listitem>
        </itemizedlist>
        <para>
            Attól függetlenül, hogy a két program kimenete ugyan az, azt kell észrevenni, hogy abstract classokat akkor használunk ha vannak funkcionalitások, amiket előre meg tudunk/akarunk adni,
            interfacet pedig, akkor ha minden-t a classban kell implementálni.
        </para>
    </section>

    <section>
        <title>Ciklomatikus komplexitás</title>
        <para>
            <command>Számoljuk ki valamelyik programunk függvényeinek ciklomatikus komplexitását! Lásd a fogalom tekintetében a https://arato.inf.unideb.hu/batfai. norbert/UDPROG/deprecated/Prog2_2.pdf (77 79 fóliát)!</command>
        </para>
        <para>
            A ciklomatikus komplexitás egy szoftver metrika, amely megadja a programunk komplexitását. Számolhatjuk a prgoramunk függvényeinek, metódusainak, osztályainak és
            moduljainak a komplexitását is külön-külön. Az alapja a gráfelméleten alapul, a kiszámításhoz először egy úgynevezett control-flow gráfot kell felrajzolni majd a
            képlet ez alapján számítható ki. A gráf a programunk futásának lehetséges útjait mutatja.
        </para>
        <para>
            A metódus, aminek nézzük a komplexitását.
        </para>
        <programlisting language="java"><![CDATA[public void pelda() {
    int i = 0;
    while(i < 10) {
        System.out.println(i++);
    }
    if (i == 10) {
        System.out.println("i = 10");
    } else {
        System.out.println("i != 10");
    }
}]]>
        </programlisting>
        <para>
            A program Control-flow gráfja (CFG):
        </para>
        <figure>
            <title>CFG</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/CFG.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Control-flow gráf</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            A gráf zöld csomópontja a metódus kezdete, a piros a vége, a kék csomópontok pedig a közbeesők. A gráfról le tudjuk olvasni a Ciklomatikus Komplexitás kiszámításához szükséges
            adatokat.
        </para>
        <para>
            A képlet: M = E − N + 2P
        </para>
        <itemizedlist>
            <listitem>
                <para>E = az élek száma, esetünkben E = 9</para>
            </listitem>
        </itemizedlist>
        <itemizedlist>
            <listitem>
                <para>N = csomópontok száma, esetünkben N = 8</para>
            </listitem>
        </itemizedlist>
        <itemizedlist>
            <listitem>
                <para>P = összekapcsolt komponensek száma, esetünkben P = 1</para>
            </listitem>
        </itemizedlist>
        <para>
            Így a képletből adódik, hogy a programunk ciklomatikus komplexitása: M = 9-8+2*1 = 3
        </para>
    </section>

    <section xml:id="liskov.sertes">
        <title>Liskov helyettesítés sértése</title>
        <para>
            <command>Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov elvet! Mutassunk rá a megoldásra: jobb OO tervezés.
                https://arato.inf.unideb.hu/batfai.norbert/PROG2/Prog2_1.pdf (93 99 fólia) (számos példa szerepel az elv megsértésére az UDPROG repóban, lásd pl. source/binom/Batfai Barki/madarak/)</command>
        </para>
        <para>
            Megoldás kódok: <link xlink:href="Liskov/liskovsertes/LiskovSert.java">Liskov/liskovsertes/LiskovSert.java</link>, <link xlink:href="Liskov/liskovsertes/LiskovKovet.java">Liskov/liskovsertes/LiskovKovet.java</link>
        </para>
        <para>
            A <filename>LiskovSert.java</filename> programban ott sértjük meg a Liskov elvet, hogy a Pingvinnek hívjuk a repul() metódusát, holott tudjuk, hogy a pingvinek nem tudnak repülni.
            A Liskov elv azt mondja ki, hogy minden osztály legyen helyettesíthető a leszármazott osztályával anélkül, hogy a program helyes működése megváltozna. Ez azért vonatkozik
            a példakódra, mert a Pingvin és Sas osztályt a Madar osztályból származtattuk és ennek a Madar osztálynak van egy olyan metódusa, hogy repul(). A Sas osztály példányosításánál nem lesz gond,
            de amikor példányosítjuk a Pingvin osztályt, akkor az örökli a repul() metódust a Madar osztályból, ez ugyan nem fog hibát okozni a program futásában (hisz az volt a cél, hogy leforduló programot írjunk),
            de elvi hibába ütközünk és ez sérti meg a Liskov elvet is, hiszen a Madar osztály nem helyettesíthető a Pingvin alosztályával emiatt az ellentmondás miatt.
        </para>
        <para>
            A probléma megoldása a <filename>LiskovKovet.java</filename>-ban látható. A Madar superclass-nak készítettünk egy RepuloMadar subclass-t, amelybe áthelyeztük a repul()
            metódust. És az olyan madarakat amik tudnak repülni ebből az osztálynól származtatjuk, a repülni képtelen madarakat, pedig a Madar classból származtatjuk.
            Így a Pingvin osztály példányosításakor nem örökli a repul() metódust (tehát meg sem tudjuk hívni), ezáltal nem ütközünk elvi hibába sem és a Liskov elv is teljesül,
            hiszen a Madar osztály helyettesíthető a leszármazott osztályaival.
        </para>
    </section>

</chapter>
