#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

int
main (void)
{
    int xj = 0, xk = 0, yj = 0, yk = 0;
    int mx; int my;

    WINDOW *ablak;
    ablak = initscr ();
    getmaxyx ( ablak, my , mx );
    my=my*2-1;
    mx=mx*2-1;

    for (;;)
    {
        xj = (xj - 1) % mx;
        xk = (xk + 1) % mx;

        yj = (yj - 1) % my;
        yk = (yk + 1) % my;

        clear ();

        for (int i=0;i<=mx;++i){
            mvprintw (0, i,"-");
            mvprintw (my/2, i,"-");
        }
        for (int i=0;i<=my;++i){
            mvprintw(i,0,"|");
            mvprintw(i,mx/2,"|");
        }

        mvprintw (abs ((yj + (my - yk)) / 2),
                  abs ((xj + (mx - xk)) / 2), "X");

        refresh ();
        usleep (100000);

    }
}
