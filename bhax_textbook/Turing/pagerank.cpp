#include <iostream>
#include <math.h>
#include <vector>

void kiir (const std::vector<double> &tomb){
	
	for (int i=0; i<tomb.size(); ++i){
		printf("%f\n",tomb[i]);
	}
}

double tavolsag (const std::vector<double> &PR, const std::vector<double> &PRv){

	double osszeg=0;
	
	for (int i = 0; i < PR.size(); ++i)
		osszeg += (PRv[i] - PR[i]) * (PRv[i] - PR[i]);
	
	return sqrt(osszeg);
}

void pagerank(std::vector<std::vector<double>> T){
    
	std::vector<double> PR = { 0.0, 0.0, 0.0, 0.0 }; //ebbe megy az eredmény
	std::vector<double> PRv = { 1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0}; //ezzel szorzok
	
	for(;;){		
		
		for (int i=0; i<4; i++){
			PR[i]=0.0;
			for (int j=0; j<4; j++){
				PR[i] += T[i][j]*PRv[j];
			}
		}
	
			if (tavolsag(PR,PRv) < 0.0000000001) 
				break;
			
			for (int i=0;i<4; i++){
				PRv[i]=PR[i];
			}	
	}
	
	kiir (PR);
}

int main (){
    
	std::vector<std::vector<double>> L = {
		{0.0,  0.0,      1.0/3.0,  0.0},
		{1.0,  1.0/2.0,  1.0/3.0,  1.0},
		{0.0,  1.0/2.0,  0.0,      0.0},
		{0.0,  0.0, 	 1.0/3.0,  0.0}
	};	
	
	printf("Az példa mátrix értékeivel történő futás:\n");
	pagerank(L);

	return 0;
}
