<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Chaitin!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Iteratív és rekurzív faktoriális Lisp-ben</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/z6NJE2a1zIA">https://youtu.be/z6NJE2a1zIA</link>      
        </para>
        <para>
            Futási videó: <link xlink:href="https://youtu.be/6Vhidakxses">https://youtu.be/6Vhidakxses</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Chaitin/fact_rec.lisp">fact_rec.lisp</link> és 
            <link xlink:href="Chaitin/fact_iter.lisp">fact_iter.lisp</link>       
        </para>
        <para>
            Mivel elsőnek találkozunk a könyvünk során a Lisppel, ezért szeretnék megemlíteni pár dolgot róla.
            A Lisp nem csak egy programozási nyelv, hanem pontosabban egy nyelvcsalád.
            Eredetileg rekurzív függvények absztrakt ábrázolására tervezték, 
            de hamarosan a mesterséges intelligencia kutatás előszeretettel alkalmazott nyelvévé vált.
            A Lisp név az angol „List Processing” (listafeldolgozás) kifejezésre vezethető vissza (maga a lisp szó angolul pöszét, pöszítést jelent.) 
            A Lisp nyelvek fő adatstruktúrája ugyanis a láncolt lsita.
            A programkód ugyanis egymásba ágyazott listák, azaz zárójelezett, ún. S-kifejezések (S-expression, sexp) sorozata.
            Fontos szintaktikai tulajdonság még, hogy az operandus előre kerül és utána jönnek az operátorok.
            A ma legelterjedtebb változatai az általános célú Common Lisp és Scheme nyelvek.
        </para>  
        <para>
            Ezen az oldalon lehet több információt megtudni a nyelvről: 
            <link xlink:href="http://nyelvek.inf.elte.hu/leirasok/Lisp/index.php">http://nyelvek.inf.elte.hu/leirasok/Lisp/index.php</link>
            vagy az irodalomjegyzékben feltöntetett forrásból.
        </para>
        <programlisting language="lisp">
<![CDATA[Rekurzív megoldás:
#!/usr/bin/clisp

(defun factorial_recursive (n)
	(if (= n 0)
		1
		(* n (factorial_recursive(- n 1)))
	)
)
		
(loop for i from 0 to 20
	do (format t "~D! = ~D~%" i (factorial_recursive i)))]]>
        </programlisting> 
        <programlisting language="lisp">
<![CDATA[Iteratív megoldás:
#!/usr/bin/clisp

(defun factorial_iterative (n)
	(let ((f 1))
		(dotimes (i n)
		(setf f (* f (+ i 1))))
		f
		)
)
	
(loop for i from 0 to 20
	do (format t "~D! = ~D~%" i (factorial_iterative i)))]]>
        </programlisting> 
        <para>
            Figyeljük meg, hogy a programok elején:
            <screen>
#!/usr/bin/clisp
            </screen>
            szerepel, tehát szükségünk lesz egy clisp package-re, ami az /usr/bin/clisp-ből elérhető lesz. 
            Így tudjuk átadni majd az interpreternek a programunkat.
        </para>
        <para>
            A programainkat futtathatóvá kell tenni, ezt a:
            <screen>
chmod 744 fact_iter.lisp fact_rec.lisp
            </screen>
            paranccsal tudjuk megtenni. Majd ./programnév alakban futtathatóak.
        </para>
        <para>
            Mindekét program 0-20-ig írja ki a számok faktoriálisát.
        </para>
        <screen>
0! = 1
1! = 1
2! = 2
3! = 6
4! = 24
5! = 120
6! = 720
7! = 5040
8! = 40320
9! = 362880
10! = 3628800
11! = 39916800
12! = 479001600
13! = 6227020800
14! = 87178291200
15! = 1307674368000
16! = 20922789888000
17! = 355687428096000
18! = 6402373705728000
19! = 121645100408832000
20! = 2432902008176640000
        </screen>
        <para>
            Ezt a feladatot meg tudjuk oldani a Gimp Script-fu konzoljában is, a következőképp:
        <screen>
(define (fakt n) (if (&lt; n 1) 1 (* n (fakt (- n 1)))))
        </screen>
            ezzel definiáltuk a rekurzív faktoriálist kiszámító függvényt és a
        <screen>
(fakt x)
        </screen>
            paranccsal pedig kiírhatjuk az "x" szám faktoriálisát. 
        </para>
        <para>
            <command>Bónusz:</command>
        </para>
        <para>
            Arr az esetre, ha a Lips-es megoldás nem lenne átlátható, akkor írtam egy <link xlink:href="Chaitin/factorial.c">C program</link>-ot, 
            hogy lehessen mivel összevetni a Lips-es programokat, hiszen ez a rekurzív és iteratív megoldást is implementálja.
        </para>
    </section>        
    
    <section xml:id="bhax-textbook-feladatok-chaitin.Chrome">
        <title>Gimp Scheme Script-fu: króm effekt</title>
        <para>
            Írj olyan script-fu kiterjesztést a GIMP programhoz, amely megvalósítja a króm effektet egy 
            bemenő szövegre!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/OKdAkI_c7Sc">https://youtu.be/OKdAkI_c7Sc</link>      
        </para>
        <para>
            Futási videó: <link xlink:href="https://youtu.be/YYRamlAAddI">https://youtu.be/YYRamlAAddI</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Chaitin/chrome.scm">chrome.scm</link> és 
            <link xlink:href="Chaitin/chrome_border.scm">chrome_border.scm</link>
        </para>
        <para>
            Ez a két program a <link xlink:href="#bhax-textbook-feladatok-chaitin.Mandala">9.3-mas feladat</link> programjából készült.
            Tehát a szerkezetük hasonló, csak más célra készült. Az átalunk megadott szövegre egy "krómozott" betűtípust rakhatunk.
        </para>
        <para>
            Futtatás:
        </para>
        <para>
            Gimp-en belül Filters->Script-Fu->Console, ezzel megnyílik a konzol. Majd a függvényeket definiáljuk, és a
            <function>script-fu-bhax-chrome</function> függvényt a megfelelő paraméterekkel meghívjuk.
        </para>
        <figure>
            <title>Chrome effect</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Chaitin/Sz_B_haxor.png" scale="200" />
                </imageobject>
                <textobject>
                    <phrase>Chrome effect</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            A <filename>chrome_border.scm</filename> annyival tud többet, a <filename>chrome.scm</filename>-től, hogy 
            keretet is készít, ugyanolyan chrome stílussal.
        </para>   
        <figure>
            <title>Chrome border</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Chaitin/chrome_border.png" scale="200" />
                </imageobject>
                <textobject>
                    <phrase>Chrome border</phrase>
                </textobject>
            </mediaobject>
        </figure> 
    </section>        

    <section xml:id="bhax-textbook-feladatok-chaitin.Mandala">
        <title>Gimp Scheme Script-fu: név mandala</title>
        <para>
            Írj olyan script-fu kiterjesztést a GIMP programhoz, amely név-mandalát készít a
            bemenő szövegből!               
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2019/01/10/a_gimp_lisp_hackelese_a_scheme_programozasi_nyelv">https://bhaxor.blog.hu/2019/01/10/a_gimp_lisp_hackelese_a_scheme_programozasi_nyelv</link>      
        </para>
        <para>
            Futási videó: <link xlink:href="https://youtu.be/BcHjG8vkFEQ">https://youtu.be/BcHjG8vkFEQ</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Chaitin/mandala.scm">mandala.scm</link>               
        </para>
        <para>
            Programértelmezés:
        </para>       
        <programlisting language="lisp">
<![CDATA[(define (elem x lista)

(if (= x 1) (car lista) (elem (- x 1) ( cdr lista ) ) )

)]]>
        </programlisting>    
        <para>
            Ezzel a függvény definícióval 2 fontos Lisp függvényt is be lehet mutatni. Mi is történik? 
            Definiáljuk az elem nevű függvényt, ami 2 paramétert kap. Ha az első paraméter (x) az 1, akkor
            hívódik a car függvény, ami a paraméterként kapott listának az első elemét adja vissza.
            Ha viszont x nem egyenlő 1-gyel, akkor rekurzívan hívja magát a függvény, első paraméterként az (x-1)-et adja,
            másodikként pedig a cdr listát. A cdr függvény az első elem kivételével visszaadja, az összes lista elemet.
            Ez a függvény arra szolgál, hogy megkapjuk egy listából az kívánt sorszámú elemet.
        </para> 
        <programlisting language="lisp">
<![CDATA[(define (text-width text font fontsize)
(let*
    (
        (text-width 1)
    )
    (set! text-width (car (gimp-text-get-extents-fontname text fontsize PIXELS font)))    

    text-width
    )
)

(define (text-wh text font fontsize)
(let*
    (
        (text-width 1)
        (text-height 1)
    )
    
    (set! text-width (car (gimp-text-get-extents-fontname text fontsize PIXELS font)))    
    
    (set! text-height (elem 2  (gimp-text-get-extents-fontname text fontsize PIXELS font)))    
       
    
    (list text-width text-height)
    )
)]]>
        </programlisting>  
        <para>
            Ezek a függvények a szöveg szélességének és magasságának a meghatározására szolgálnak.
        </para>
        <programlisting language="lisp">
<![CDATA[(define (script-fu-bhax-mandala text text2 font fontsize width height color gradient)]]>
        </programlisting> 
        <para>
            A <function>script-fu-bhax-mandala</function> függvény lesz a fő függvényünk, amit meghívva a konzolból,
            tudjuk kirajzoltatni a képet. 8 paramétert kell megadnunk. A text lesz az a szöveg, amit "mandalává" szeretnénk tenni,
            tehát, amit elforgatva többször látunk. A text2 a kép közepére írt szöveg, amelynek a betűtípusát a font paraméter adja,
            a méretét pedig a fontsize. A width lesz a képünk szélessége pixelekben, a height pedig a magassága. A color RGB-ben megadva
            a text 2 színe, a gradient pedig a text-nek a mintája.
        </para>
        <programlisting language="lisp">
<![CDATA[(script-fu-register "script-fu-bhax-mandala"
    "Mandala9"
    "Creates a mandala from a text box."
    "Norbert Bátfai"
    "Copyright 2019, Norbert Bátfai"
    "January 9, 2019"
    ""
    SF-STRING       "Text"      "Bátf41 Haxor"
    SF-STRING       "Text2"     "BHAX"
    SF-FONT         "Font"      "Sans"
    SF-ADJUSTMENT   "Font size" '(100 1 1000 1 10 0 1)
    SF-VALUE        "Width"     "1000"
    SF-VALUE        "Height"    "1000"
    SF-COLOR        "Color"     '(255 0 0)
    SF-GRADIENT     "Gradient"  "Deep Sea"
)
(script-fu-menu-register "script-fu-bhax-mandala" 
    "<Image>/File/Create/BHAX"
)]]>
        </programlisting>
        <para>
            A program ezen része pedig a menüben elérhetővé teszi, hogy könnyedén tudjuk állítani a paramétereket a Gimpen belül.
            File->Create->BHAX.
        </para>
        <para>
            Futtatás saját paraméterekkel (hasonlóan a <link xlink:href="#bhax-textbook-feladatok-chaitin.Chrome">9.2-ben</link> leírt futtatással):
        </para>
        <screen>
(script-fu-bhax-mandala "Szabó Bence" "BHAXOR" "Ruge Boogie" 120 700 700 '(0 191 255) "Deep Sea")
        </screen>
        <figure>
            <title>Szabó Bence BHAXOR mandala</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Chaitin/sajat_mand_atl.png" scale="200" />
                </imageobject>
                <textobject>
                    <phrase>Szabó Bence BHAXOR mandala</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            Annyi változtatás van a képen, hogy a program által készített kép után a fekete részt átlátszóvá tettem,
            hogy akár logónak is jól használható legyen.
        </para>
        <para>
            Fontos még megemlíteni, hogy a betűtípus megadásakor a Ruge Boogie-t adtam meg, ami nem beépített betűtípusa a Gimp-nek.
            A <link xlink:href="Chaitin/RugeBoogie-Regular.ttf">RugeBoogie-Regular.ttf</link> fájlt használtam.
            A megfelelő mappába (Ubuntun, Gimp 2.10-zel: ~/snap/gimp/252/.config/GIMP/2.10/fonts) betéve a fájlt fel fogja ismerni a font-ot.
        </para>
    </section> 
          
    <section>
        <title>Vörös Pipacs Pokol/SmartSteve tovább javítás</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/5k3hw25_DVA">https://youtu.be/5k3hw25_DVA</link>
        </para>   
        <para>
            Megoldás forrása: <link xlink:href="Chaitin/smart_steve_sajat2.py">smart_steve_sajat2.py</link>               
        </para>     
        <para>
            Ez a program a <link xlink:href="#schwarzenegger.smart_sajat">SmartSteve javítás</link> feladat
            megoldásának a továbbfejlesztése. Az egyetlen új dolog, amit implementáltam, az az volt, hogy menjen fel az elején
            pár szintet és felülről kezdje a virágkeresést. Természetesen, ehhez kellett optimalizálni a programot
            tehát a felmenés helyett lefele jöjjön, stb.
        </para>    
        <programlisting language="python">
<![CDATA[Felmegy függvény beépítése

def felmegy(self):
    for i in range(20):
        self.agent_host.sendCommand( "move 1" )
        time.sleep(0.01)
        self.agent_host.sendCommand( "jumpmove 1" )
        time.sleep(0.01)

def run(self):
    world_state = self.agent_host.getWorldState()
    # Loop until mission ends:
    self.agent_host.sendCommand( "look 1" )
    self.agent_host.sendCommand( "look 1" )
    self.felmegy()
    while world_state.is_mission_running:
    
        #print(">>> nb4tf4i arena -----------------------------------\n")
        act = self.action(world_state)
        #print("nb4tf4i arena >>> -----------------------------------\n")
        if not act:
            self.idle(.017)
        world_state = self.agent_host.getWorldState()]]>
        </programlisting>
        <programlisting language="python">
<![CDATA[lvlUp lecserélése lvldown-ra

def lvldown(self, nbr):
    if self.collectedFlowers[self.y]:
        self.turnFromWall(nbr)
        self.agent_host.sendCommand( "strafe 1" )
        time.sleep(.2)     
        self.agent_host.sendCommand( "strafe 1" )
        time.sleep(.2)         
        return True
    else:
        return False]]>
        </programlisting>
    </section>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
</chapter> 