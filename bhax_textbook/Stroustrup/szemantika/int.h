#ifndef INT_H		//ha nincs definiálva if not define
#define INT_H		//akkor definiáljuk

#include <iostream>

class Int{

private:
    int *value;   //dinamikus tag a pointer, enéljül nincs értelme a szemantikának
    int bogoc;    //számláló, hanyadjára jött létre az aktuális objektum

public:
    static int bogoCount;

    Int(int value = 0) : value(new int(value)) {	

        bogoc = bogoCount++;

        std::cout << "Int ctor " << bogoc << " " << *this->value << " " << this->value << " " << this << std::endl;
    
    }

    Int(const Int & old) : value(new int(*old.value)){	

        bogoc = bogoCount++;

        std::cout << "Int copy ctor " << bogoc << " " << *this->value << " " << this->value << " " << this << std::endl;

    }

    Int & operator=(const Int & old){  	//másoló értékadás Int referenciát visszaadó operator a fgv neve, a 						  ()ben pedig a paraméterek
        Int tmp{old};	//hívjuk a másoló konstruktort, mert ez értékadás lényegében

        std::swap(*this,tmp);	//innen megy a mozgató konstruktorra, az implementációban látszódik az '=' 					  jelnél, a move.h-ban van, az std::move-ot hívja igazából makróval

        std::cout << "Int copy assign " << bogoc << " " << *this->value << " " << this->value << " " << this << std::endl;
    
        return *this;
    }

        //jobb érték referencia_ &&
    Int(Int && old) {		//mozgató konstruktor, a másoló értékadásra építjük rá

        bogoc = bogoCount++;
				//a swapből az első sorban vagyunk
        value = nullptr;
        *this = std::move(old);	//innen az értékadásra a mozgató szemantikában

        std::cout << "Int move ctor " << bogoc << " " << *this->value << " " << this->value << " " << this << std::endl;

    }

    Int & operator=(Int && old){	

        std::swap(value,old.value);	//ez lefut és visszamegy			
					//a swap miatt (a move.h-ban) jön két mozgató értékadás
        std::cout << "Int move assign " << bogoc << " " << *this->value << " " << this->value << " " << this << std::endl;
    
        return *this;
    }

    ~Int(){	
        
        delete value;		//az első sorból a tmp törlődik

        std::cout << "Int dtor " << bogoc << " "  << this->value << " " << this << std::endl;
	
    }

};

#endif		//ifndef-nek a vége, hiszen az egy feltétel volt, erre a párosra nem feltétlen lenne szükség