#include "int.h"

int main(){
    /*
    Int a{42};
    Int b{7};

    a=b;	//ez másoló értékadás mert a b értéke is megmarad
	*/

    /*
    Int a=2; 
    Int b = std::move(a); //bővítés
    */
    
    Int a=2; 
    Int b=3; 
    b = std::move(a);
    
    return 0;
}