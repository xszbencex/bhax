#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include <boost/filesystem.hpp>
#include <iostream>

namespace fs = boost::filesystem;
using namespace std;

vector<fs::path> get_all(fs::path const & root, string const & ext)
{
    vector<fs::path> paths;

    if (fs::exists(root) && fs::is_directory(root))
    {
        for (auto const & entry : fs::recursive_directory_iterator(root))
        {
            if (fs::is_regular_file(entry) && entry.path().extension() == java)
                paths.emplace_back(entry.path().filename());
        }
    }

    return paths;
} 


int main() {
    vector<fs::path> files = get_all("C:\\Java\\classes", ".java");
    for (auto i = files.begin(); i != files.end(); ++i)
        std::cout << *i << ' ';
    return 0;
}