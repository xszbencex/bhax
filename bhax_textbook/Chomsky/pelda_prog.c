#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

void jelkezelo(){
    printf("\nJel érkezett\n");
    exit(0);
}

int f (int x, int y){
	printf("f-ben: %d %d\n",x,y);
	return x+y;
}

int g (int x){
	return ++x;
}

int h(int *x){
	return ++ *x;
}

int main()
{
   if (signal(SIGINT, jelkezelo)==SIG_IGN)
   	signal(SIGINT, SIG_IGN);
   	
	int i;
	for(i=0; i<5; ++i)
		printf("%d\n",i);
	
	int tomb[5];
	int t[5], t2[5];
	int *s = t;
	int *d = t2;
	
	for(i = 0; i < 5 && (*d++ = *s++); i++)
		printf("%d\n",i);
	
	for(i=0; i<5; tomb[i] = i++)
		printf("%d\n", tomb[i]);

	int a=0;
	printf("f-en kívül:%d %d\n",f(a, ++a), f(++a, a));

	printf("g-n kívül%d %d\n",g(a), a);

	printf("h-n kívül%d %d\n",h(&a), a);
	
	printf("\n");

	a=0;
	printf("f-en kívül:%d %d\n",f(++a, a), f(a, ++a));

	printf("g-n kívül:%d %d\n",a, g(a));

	printf("h-n kívül:%d %d\n",a, h(&a));

   for (;;){}
    
   return 0;
}
