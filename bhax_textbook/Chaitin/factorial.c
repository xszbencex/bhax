#include <stdio.h>

#define n 20

long int fact_rec(int x){
    if (x<1)
        return 1;
    else 
        return x*fact_rec(x-1);
}

long int fact_iter(int x){
    long int a=1;
    if (x<1)
        return 1;
    else {
        for (int i=1; i<=x; ++i)
            a*=i;
    }
    return a;
}

int main(){
    printf("Faktoriális rekurzívan\n");
    for (int i=0; i<=n; ++i)
        printf("%d!\t= %ld\n",i,fact_rec(i));
    
    printf("Faktoriális iteratívan\n");
    for (int i=0; i<=n; ++i)
        printf("%d!\t= %ld\n",i,fact_iter(i));
    
}
