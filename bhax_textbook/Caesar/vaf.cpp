//Változó argomentumszámú függvény
#include <stdio.h>
#include <stdarg.h> //itt van definialva

double atlag(int num, ... ){
	
	va_list arguments; //ebben lesznek az argomentumok
	double sum = 0.0;
	
	va_start (arguments, num); // inicializáljuk a listát, a num után vannak a változó argomentumszámok, 
										//tehát azt kell megadni 	honnan indulnak
	for (int i = 0; i < num; i++)
		sum +=va_arg(arguments, double); //miből kérjük az argomentumokat, és milyen típusú, 
													//szerencsére itt ugyaolyan mind
	va_end(arguments);
	
	return sum / num;
}

int main()
{
	printf("%f\n", atlag(3, 53.1, 12.4, 43.2));
	printf("%f\n", atlag(5, 200.12, 62.3, 22.2, 100.0, 42.2));
	return 0;
}
