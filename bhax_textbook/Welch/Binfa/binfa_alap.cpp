#include<iostream>

template <typename ValueType>       //azért valuetype és nem konkrét típus, mert változik
class BinTree {        //C-ben struktúra

protected:             //protectedet örökli a gyerek is

    class Node {       //csomópont
    
    private:                //Node privát része
        ValueType value;    //az érték ami változhat hogy milyen típusú
        Node *left;
        Node *right;
        int count{0};       //hányszor szerepelt már az elem (ZLW-ben mindig 1-szer szerepel 1 elem)

        //TODO rule of five   -   ha egy osztlyban van pointer akkor kell destruktor    
        //itt tiltunk a private miatt
        Node(const Node &);                 
        Node & operator=(const Node &);
        Node(const Node &&);                //jobb érték, mozgató konstr.
        Node & operator=(const Node &&);    

    public:
        Node(ValueType value): value(value), left(nullptr), right(nullptr) {} //konstruktor
        
        ValueType getValue(){return value;}     //{} között az implementálás is megtörténik, 
        Node * leftChild(){return left;}        //a template osztályok miatt van rá szükség
        Node * rightChild(){return right;}      //nem szedjük szét headerbe
        void leftChild(Node * node){left=node;}
        void rightChild(Node * node){right=node;}
        int getCount(){return count;}
        void incCount(){++count;}
    };
    
    Node *root;         //a protectedben vagyunk  
    Node *treep;        //hol tartunk
    int depth{0}; 

private:                              //fa privát része    itt tiltunk private miatt
    BinTree(const BinTree &);       
    BinTree & operator=(const BinTree &);        
    BinTree(BinTree &&);
    BinTree & operator=(BinTree &&);

public:
    BinTree(Node *root =nullptr, Node *treep=nullptr): root(root), treep(treep){}      //fa konstruktora, a ':' után a konstruktor argomentumlistája van, a nullptr-ek kezdő értékadások
    
    ~BinTree(){         //destruktor
        deltree(root);  //deltree deklarálás
    }
    BinTree & operator<<(ValueType value);      //hogy a shiftelés működjön
    void print(){print(root, std::cout);}
    void print(Node *node, std::ostream & os);
    void deltree(Node *node);
};

template <typename ValueType>                   //mindig közölni kell a templatet
class ZLWTree : public BinTree<ValueType> {     //kiterjesztés

public:
    ZLWTree(): BinTree<ValueType>(new typename BinTree<ValueType>::Node('/')){  //binfa konstruktorát hívom, az ősosztály a binfa, röptében csináltuk a rootot
        this->treep = this->root;
    } 
    ZLWTree & operator<<(ValueType value);     
};

template <typename ValueType>
BinTree<ValueType> & BinTree<ValueType>::operator<<(ValueType value){       //shiftelés implementálása
    if(!treep){                                 //itt építi fel a fát
        root = new Node(value);

    } else if (treep->getValue() == value){
        treep->incCount();

    } else if (treep->getValue() > value){
        if(!treep->leftChild()){
            treep->leftChild(new Node(value));

        } else {
            treep = treep->leftChild();
            *this << value;
        }

    } else if (treep->getValue() < value){
        if(!treep->rightChild()){
            treep->rightChild(new Node(value));

        } else {
            treep = treep->rightChild();
            *this << value;
        }
    }
    treep=root;
    return *this;
}

template <typename ValueType>
ZLWTree<ValueType> & ZLWTree<ValueType>::operator<<(ValueType value){       //ZLWTree<ValueType> & értéket ad vissza, ZLWTree<ValueType> a metódus
    
    if(value=='0'){                     //ZLW felépítés

        if(!this->treep->leftChild()){
            typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);        //hivatkozás a Binfa Node classjára
            this->treep->leftChild(node);
            this->treep = this->root;

        }else{
            this->treep = this->treep->leftChild();
        }

    }else {

        if(!this->treep->rightChild()){
            typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
            this->treep->rightChild(node);
            this->treep = this->root;

        }else{
            this->treep = this->treep->rightChild();
        }
    }

    return *this;
}

template <typename ValueType>
void BinTree<ValueType>::print(Node *node, std::ostream & os){
    if(node){
        ++depth;
        print(node->rightChild(),os);

        for(int i{0}; i<depth; ++i)
            os << "---";

        os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;

        print(node->leftChild(),os);
        --depth;
    }
}

template <typename ValueType>
void BinTree<ValueType>::deltree(Node *node){

    if(node){
        deltree(node->leftChild());
        
        deltree(node->rightChild());

        delete node;

    }
}

int main(int argc, char** argv, char ** env){       //env: környezeti változó
    BinTree<int> bt;        //template paraméter az int a valuetype lesz int
    
    bt << 8 << 9 << 5 << 2 << 7;

    bt.print();

    std::cout << std::endl;

    ZLWTree<char> zt;

    zt << '0' <<'1'<<'1'<<'1'<<'1'<<'0'<<'0'<<'1'<<'0'<<'0'<<'1'<<'0'<<'0'<<'1'<<'0'<<'0'<<'0'<<'1'<<'1'<<'1';
    
    zt.print();
}