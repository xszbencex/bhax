package Godel.refactor;

public interface Calculator {

    Integer calculate(Integer number);

}
