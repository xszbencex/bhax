package Godel.list;

import java.util.ArrayList;
import java.util.LinkedList;

import Godel.list.measurement.*;
import Godel.list.meter.ListMeter;

public class Main {

    private static final int SIZE_10M = 10_000_000;
    private static final int SIZE_100K = 100_000;
    
    public static void main(String[] args) {
        System.out.println(" --- List measurement ---");
        new Measurement(
                new ListMeter(() -> new ArrayList<>()),
                new ListMeter(() -> new ArrayList<>(SIZE_10M)),
                new ListMeter(() -> new LinkedList<>())
        ).measureCollectionSetup(SIZE_10M)
                .measureFrequentChanges(SIZE_100K)
                .measureGet(SIZE_10M);
    }
}
