package Liskov.interfesz;

public class InterfacePelda {
    interface Shape {
        void draw();
        double area();
    }

    static class Rectangle implements Shape {
        int length, width;

        Rectangle(int length, int width) {
            this.length = length;
            this.width = width;
        }

        @Override
        public void draw() {
            System.out.println("Rectangle has been drawn ");
        }

        @Override
        public double area() {
            return (double)(length*width);
        }
    }

    public static void main (String[] args) {
        Shape rect = new Rectangle(2,3);
        System.out.println("Area of rectangle: " + rect.area());
        rect.draw();
    }
}
