package Liskov.interfesz;

public class AbstractClassPelda {

    static abstract class Shape {
        String objectName = "";

        Shape(String name)
        {
            this.objectName = name;
        }

        public void draw() {
            System.out.println("Rectangle has been drawn ");
        }

        abstract public double area();
    }

    static class Rectangle extends Shape {
        int length, width;

        Rectangle(int length, int width, String name) {
            super(name);
            this.length = length;
            this.width = width;
        }

        @Override
        public double area() {
            return (double)(length*width);
        }
    }

    public static void main (String[] args) {
        Shape rect = new Rectangle(2,3, "Rectangle");
        System.out.println("Area of rectangle: " + rect.area());
        rect.draw();
    }
}
