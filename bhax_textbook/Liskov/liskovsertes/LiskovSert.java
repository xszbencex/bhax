package Liskov.liskovsertes;

public class LiskovSert {
    static class Madar {
         public void repul() {};
    };

    static class Sas extends Madar {
    }

    static class Pingvin extends Madar {
    }

    public static void main(String[] args) {
        Madar madar = new Madar();
        madar.repul();

        Sas sas = new Sas();
        sas.repul();

        Pingvin pingvin = new Pingvin();
        pingvin.repul(); 
    }
}
