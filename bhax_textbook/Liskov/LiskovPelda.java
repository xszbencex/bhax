public class LiskovPelda {

    static class Vehicle {

        public Vehicle() {
            System.out.println("I am a vehicle.");
        }

        void start() {
            System.out.println("Vehicle started");
        }
    }

    static class Car extends Vehicle {
        public Car() {
            System.out.println("I am a car");
        }

        @Override
        void start() {
            System.out.println("Car started");
        }
    }

    static class Supercar extends Car {
        public Supercar() {
            System.out.println("I am a Supercar");
        }

        @Override
        void start() {
            System.out.println("Supercar started");
        }
    }

    public static void main(String[] args) {
        Vehicle firstVehicle = new Supercar();
        firstVehicle.start();
        System.out.println(firstVehicle instanceof Car);

        Car secondVehicle = (Car) firstVehicle;
        secondVehicle.start();
        System.out.println(secondVehicle instanceof Supercar);

        // Supercar thirdVehicle = new Vehicle();
        // thirdVehicle.start();
    }
}
