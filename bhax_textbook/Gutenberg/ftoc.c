#include<stdio.h>
/* Fahrenheit-Celsius táblázat*/
int main(){
	int lower, upper, step;		//egész (integer) típusú deklaráció
	float fahr, celsius;		//lebegőpontos (float) (típusú deklaráció
	lower = 0; 			//alsó határ
	upper = 300;			//felső határ
	step = 20; 			//lépésköz
	fahr = lower;			//értékadás
	printf("%4c %6c\n",'F','C'); 	//fejléc
	while (fahr <=upper) {		
		celsius = (5.0 / 9.0)*(fahr-32.0);	//celsius számítás
		printf("%4.0f %6.1f \n", fahr, celsius);	
		fahr += step;
		}
}
