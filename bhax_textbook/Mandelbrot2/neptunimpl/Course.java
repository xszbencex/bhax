package Mandelbrot;

import java.util.ArrayList;
import java.util.List;

public class Course {

    private String courseId;
    private Lecturer lecturer;
    private String date;
    private Integer capacity;
    private Subject subject;
    private CourseType courseType;
    private List<Student> students = new ArrayList<Student>();

    public Course(String courseId, Lecturer lecturer, String date, Integer capacity, Subject subject, CourseType courseType) {
        this.courseId = courseId;
        this.lecturer = lecturer;
        this.date = date;
        this.capacity = capacity;
        this.subject = subject;
        this.courseType = courseType;
    }

    public String getCourseId() {
        return courseId;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public String getDate() {
        return date;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Subject getSubject() {
        return subject;
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student student) {
        this.students.add(student);
        --this.capacity;
    }

    public void removeStudent(Student student) {
        this.students.remove(student);
        ++this.capacity;
    }
}
