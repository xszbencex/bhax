package Mandelbrot;

public enum SubjectType {
    OBLIGATORY,
    OPTIONAL,
    ELECTIVE
}
