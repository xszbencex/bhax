public class Pelda {

    static class Nev {

        private String firstName;
        private String lastName;

        public Nev(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "Nev{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Nev nev = (Nev) o;

            if (firstName != null ? !firstName.equals(nev.firstName) : nev.firstName != null) return false;
            return lastName != null ? lastName.equals(nev.lastName) : nev.lastName == null;
        }

        @Override
        public int hashCode() {
            int result = firstName != null ? firstName.hashCode() : 0;
            result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
            return result;
        }
    }

    public static void main(String[] args) {
        Nev ember1 = new Nev("Bence", "Szabó");
        Nev ember2 = new Nev("Bence", "Szabó");
        System.out.println(ember1.equals(ember2));
        System.out.println(ember1.toString());
    }
}
