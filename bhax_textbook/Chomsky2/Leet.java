package Chomsky2;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Leet {

    static Map map = new HashMap<Character, String[]>();
    static String inputText = "";
    static String leetText = "";

    static void readInput() {
        Scanner scanner = new Scanner(System.in);
        inputText = scanner.nextLine();
    }

    static void mapFill() {
        map.put('A', new String[] {"4", "/-\\", "/_\\", "@", "/\\", "Д", "а"});
        map.put('B', new String[] {"8", "|3", "13", "|}", "|:", "|8", "18", "6", "|B", "|8", "lo", "|o", "j3", "ß", "в", "ь"});
        map.put('C', new String[] {"<", "{", "[", "(", "©", "¢", "с"});
        map.put('D', new String[] {"|)", "|}", "|]", "|>"});
        map.put('E', new String[] {"3", "£", "₤", "€","е"});
        map.put('F', new String[] {"|=", "ph", "|#", "|\"\"", "ƒ"});
        map.put('G', new String[] {"[", "-", "[+", "6", "C-"});
        map.put('H', new String[] {"#", "4", "|-|", "[-]", "{-}", "}-{", "}{", "|=|", "[=]", "{=}", "/-/", "(-)", ")-(", ":-:", "I+I", "н"});
        map.put('I', new String[] {"1", "|", "!", "9"});
        map.put('J', new String[] {"_|", "_/", "_7", "_)", "_]", "_}"});
        map.put('K', new String[] {"|<", "1<", "l<", "|{", "l{"});
        map.put('L', new String[] {"|_", "|", "1", "]["});
        map.put('M', new String[] {"44", "|\\/|", "^^", "/\\/\\", "/X\\", "[]\\/][", "[]V[]", "][\\\\//][", "(V)","//.", ".\\\\", "N\\", "м"});
        map.put('N', new String[] {"|\\|", "/\\/", "/V", "][\\\\][", "И", "и", "п"});
        map.put('O', new String[] {"0", "()", "[]", "{}", "<>", "Ø", "oh", "Θ", "о", "ө"});
        map.put('P', new String[] {"|o", "|O", "|>", "|*", "|°", "|D", "/o", "[]D", "|7", "р"});
        map.put('Q', new String[] {"O_", "9", "(,)", "0", "kw"});
        map.put('R', new String[] {"|2", "12", ".-", "|^", "l2", "Я", "®"});
        map.put('S', new String[] {"5", "$", "§"});
        map.put('T', new String[] {"7", "+", "7`", "');|'" , "`|`" , "~|~" , "-|-", "']['", "т"});
        map.put('U', new String[] {"|_|", "\\_\\", "/_/", "\\_/", "(_)", "[_]", "{_}"});
        map.put('V', new String[] {"\\/"});
        map.put('W', new String[] {"\\/\\/", "(/\\)", "\\^/", "|/\\|", "\\X/", "\\\\'", "'//", "VV", "\\_|_/", "\\\\//\\\\//", "Ш", "2u", "\\V/"});
        map.put('X', new String[] {"%", "*", "><", "}{", ")(", "Ж"});
        map.put('Y', new String[] {"`/", "¥", "\\|/", "Ч", "ү", "у"});
        map.put('Z', new String[] {"2", "5", "7_", ">_","(/)"});
        map.put('1', new String[] {"I", "L", "7"});
        map.put('2', new String[] {"Z"});
        map.put('3', new String[] {"E", "e", "m", "w", "ω", "∈", "ε", "∩∩"});
        map.put('4', new String[] {"h", "A"});
        map.put('5', new String[] {"S"});
        map.put('6', new String[] {"b", "G"});
        map.put('7', new String[] {"T", "j", "L"});
        map.put('8', new String[] {"B", "X"});
        map.put('9', new String[] {"g", "J"});
        map.put('0', new String[] {"C", "O", "D", "Θ", "o"});
    }

    static void createOutput() {
        Random rand = new Random();
        for (int i = 0; i < inputText.length(); i++) {
            Character character = inputText.charAt(i);
            if (map.containsKey(character)) {
                int randNum = rand.nextInt(((String[]) map.get(character)).length);
                leetText += ((String[]) map.get(character))[randNum];
            } else {
                leetText += character;
            }
        }
    }

    public static void main(String[] args) {
        System.out.print("Enter text (uppercase):\t");
        readInput();
        mapFill();
        createOutput();
        System.out.println("Text in leetspeak: \t \t" + leetText);
    }
}
